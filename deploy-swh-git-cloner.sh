#!/usr/bin/env bash

# script relies on ~/.ssh/config file

set -x

# awork0* for azure ; worker0* for swh
WORKER_HOST=${1-"worker01"}
# swhworker for swh
# antelink for azure
# USER_HOST=${2-"swhworker"}

rsync -avz -e 'ssh' ./install-new-code-and-keep-log.sh $WORKER_HOST:
ssh -tt $WORKER_HOST ./install-new-code-and-keep-log.sh
