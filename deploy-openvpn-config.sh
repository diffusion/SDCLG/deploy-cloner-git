#!/usr/bin/env bash

# Install the needed pre-requisites to antelink's node (openvpn install + conf)
# The expected configuration files are supposed to be ok locally first
# then uploaded to the node (via this script).

set -x

# Expected host awork01, awork02, awork03, awork04, awork05 with
# respective configurations ok in ~/.ssh/config file
HOST=$1
TEMP_FOLDER=/tmp/$HOST

rexec="ssh -tt $HOST"

# Install openvpn on remote host (expected sudo rights...)
$rexec sudo yum install -y openvpn

# Upload the openvpn folder configuration file
scp -r ./openvpn/$HOST $HOST:$TEMP_FOLDER
scp -r ./openvpn/softwareheritage-ca.crt $HOST:$TEMP_FOLDER

# Prepare the needed folder
$rexec sudo mkdir -p /etc/openvpn/keys/

# Install files remotely
$rexec sudo cp $TEMP_FOLDER/$HOST.key $TEMP_FOLDER/$HOST.crt $TEMP_FOLDER/softwareheritage-ca.crt /etc/openvpn/keys/
$rexec sudo cp $TEMP_FOLDER/softwareheritage.conf /etc/openvpn/

# clean up
$rexec sudo rm -rf $TEMP_FOLDER
