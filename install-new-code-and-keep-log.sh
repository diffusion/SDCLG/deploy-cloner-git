#!/usr/bin/env bash

set -x

# clean up stale data
# rm -rf /mnt/resource/swh-git-cloner
# mkdir -p /mnt/resource/swh-git-cloner

# stop the potential running worker
pkill celery

SWH_ENV_HOME=~/swh-environment

if [ ! -d $SWH_ENV_HOME ]; then
    git clone ssh://git@forge.softwareheritage.org/diffusion/DENV/swh-environment.git $SWH_ENV_HOME
fi

cd $SWH_ENV_HOME
git pull
mr update
