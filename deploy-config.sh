#!/usr/bin/env bash

# relies on .ssh/config file for access to remove workers
set -x

WORKER_HOST=${1-"worker01"}

# ssh $WORKER_HOST "rm -rf .config/swh && mkdir -p .ssh .config tmp"
rsync -avz -e 'ssh' .config/ ${WORKER_HOST}:.config/
# rsync -avz -e 'ssh' ./.ssh/config ${WORKER_HOST}:.ssh/config
# rsync -avz -e 'ssh' ./etc/hosts ${WORKER_HOST}:
# ssh -tt $WORKER_HOST cat $HOME_HOST/hosts | sudo tee -a /etc/hosts
# rsync -avz -e 'ssh' ./.ssh/ ${WORKER_HOST}:$HOME_HOST/.ssh/
# rsync -avz -e 'ssh' ./.ssh/swh/ ${WORKER_HOST}:.ssh/swh/
